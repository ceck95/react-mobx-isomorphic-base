const webpack = require('webpack');
const path = require('path');
const ExtractCSS = require('extract-text-webpack-plugin');
const VersionFile = require('webpack-version-file');

//ConfigServer
const configServer = require('./server.config');
const fileName = configServer.webpack.fileName;
const destinationGen = configServer.http.static[0].url.replace('/', '');

// Define client source path
const sources = path.join(__dirname, '../src/client');

//set default environment
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
//Build version
const timeCurrent = new Date();
const versionCurrent = timeCurrent.getTime();
const timeString = `${timeCurrent.getHours()}:${timeCurrent.getMinutes()}:${timeCurrent.getSeconds()}  ${timeCurrent.getDate()}/${timeCurrent.getMonth() +
  1}/${timeCurrent.getFullYear()}`;
const isPro = process.env.NODE_ENV === 'production';
const parseName = (kind, extname) => {
  return `${kind}${isPro ? `-${versionCurrent}` : ''}.${extname}`;
};
const pathNodeModules = path.join(__dirname, '..', 'node_modules');
const pathSCSS = path.join(__dirname, '..', 'src', 'client', 'assets', 'css');

// Default config
const config = {
  mode: process.env.NODE_ENV,
  entry: './index.js',
  target: 'web',
  context: sources,
  node: {
    global: true,
    fs: 'empty'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        include: sources,
        query: {
          cacheDirectory: false,
          presets: ['es2016', 'react']
        }
      },
      {
        test: /\.(css|scss|sass)(\?.+)?$/,
        include: [pathNodeModules, pathSCSS],
        use: ExtractCSS.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'resolve-url-loader', 'sass-loader?sourceMap']
        })
      },
      // {
      //   test: /\.(jpe|jpg|woff|woff2|eot|ttf|svg)(\?.*$|$)/,
      //   loader: 'url-loader'
      // },
      // {
      //   test: /\.(jpe|jpg|woff|woff2|eot|ttf|svg)(\?.*$|$)/,
      //   use: [
      //     {
      //       loader: 'file-loader',
      //       options: {
      //         name: '[hash].[ext]',
      //         outputPath: 'assets/',
      //         publicPath: '/'
      //       }
      //     }
      //   ]
      // },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader?limit=10000&minetype=application/font-woff',
            options: {
              publicPath: '/public'
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              publicPath: '/public'
            }
          }
        ]
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, '..', destinationGen),
    filename: parseName(fileName.js, 'js'),
    publicPath: '/'
  },
  plugins: [
    //new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/), // If you don't need moment's locales
    new ExtractCSS(parseName(fileName.css, 'css'), {
      allChunks: true
    })
  ]
};

//Case production
if (isPro)
  config.plugins.push(
    new VersionFile({
      output: './version.json',
      data: {
        date: versionCurrent,
        environment: process.env.NODE_ENV || 'development',
        time: timeString
      },
      templateString: `{
        "version": "<%= date %>",
        "environment": "<%= environment %>",
        "time":"<%= time %>"
      }`
    })
  );

module.exports = config;
