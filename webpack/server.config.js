const path = require('path');
let config = require(`../config/${process.env.NODE_ENV || 'development'}.json`);
const configDefault = require('../config/default.json');
config = Object.assign(config, configDefault);
if (config.http) {
  config.http = Object.assign(
    {
      favicon: path.join(__dirname, '..', 'src/client/assets/favicon.ico'),
      robots: 'User-agent: *\nDisallow:'
    },
    config.http
  );
  if (Array.isArray(config.http.static) && config.http.static.length > 0)
    config.http.static = config.http.static.map(e => {
      return {
        url: e.url,
        path: path.join(__dirname, '..', `${e.url.replace('/', '')}/`)
      };
    });
}

module.exports = config;
