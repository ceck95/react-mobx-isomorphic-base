const webpack = require('webpack');
const config = require('./webpack.config.base.js');
const configServer = require('./server.config');
const fileName = configServer.webpack.fileName;
const httpPort = configServer.http.port;
const httpHost = configServer.http.host || 'localhost';
const path = require('path');

// Use port ( if you change this adapt code in Html.jsx as well )
const devPort = configServer.webpack.devPort;

//Auto open browser
// const OpenBrowserPlugin = require('open-browser-webpack-plugin');
// config.plugins.push(
//   new OpenBrowserPlugin({ url: `http://${httpHost}:${httpPort}` })
// );

// Setup webpack for DEV
//-------------------------------
const compiler = webpack(
  Object.assign(config, {
    cache: false,
    devtool: 'source-map',
    entry: {
      main: [
        // 'webpack/hot/dev-server',
        `webpack-dev-server/client?http://localhost:${devPort}/`,
        'babel-polyfill',
        config.entry
      ]
    }
  })
);
const compilerConfig = {
  compress: true,
  watchOptions: {
    poll: true,
    aggregateTimeout: 500,
    ignore: /node_modules|data|build|\.git/
  },
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    'Access-Control-Allow-Headers':
      'X-Requested-With, content-type, Authorization'
  },
  host: 'localhost',
  disableHostCheck: true,
  port: devPort,
  stats: {
    colors: true,
    version: true,
    hash: false,
    timings: true,
    chunks: false,
    chunkModules: false,
    modules: false,
    children: false
  }
};

// Launch DEV server
//-------------------------------
console.info('webpack: running dev build...');

const WebpackDevServer = require('webpack-dev-server');
const server = new WebpackDevServer(compiler, compilerConfig);

compiler.plugin('done', stats => {
  stats = stats.toJson();
  if (stats.errors && stats.errors.length > 0) {
    stats.errors.forEach(e => {
      console.log(JSON.stringify(e));
    });
    return false;
  }

  server.listen(devPort, 'localhost', () => {
    console.info(`webpack: dev server running on port ${devPort}`);
  });
});

module.exports = compiler;
