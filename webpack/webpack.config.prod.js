/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-12 10:49:59 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-04-20 15:09:01
 */
const webpack = require('webpack');
const config = Object.assign(require('./webpack.config.base.js'), {
  cache: false
});
const path = require('path');
const versionCurrent = require('../version.json').version;
const fileName = require('./server.config').webpack.fileName;
const fs = require('fs');
const fileBundle = [
  `${fileName.js}-${versionCurrent}.js`,
  `${fileName.css}-${versionCurrent}.css`
];
const pathGen = config.output.path;
if (pathGen)
  fs.readdir(pathGen, (err, files) => {
    if (err) console.error(err);
    files.forEach(e => {
      if (fileBundle.includes(e))
        fs.unlink(`${pathGen}/${e}`, err => {
          if (!err) console.log(`Delete file successfully ${e}`);
        });
    });
  });

// Merge with base configuration
//-------------------------------
delete config.output.libraryTarget;
delete config.output.pathinfo;

//plugin webpack
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

// Save files to disk
//-------------------------------
config.plugins.push(
  new webpack.DefinePlugin({
    'process.env.BLUEBIRD_WARNINGS': '0',
    'process.env.NODE_ENV': JSON.stringify('production')
  }),
  new webpack.optimize.OccurrenceOrderPlugin(),
  // new webpack.optimize.DedupePlugin(),
  new UglifyJsPlugin({
    uglifyOptions: {
      mangle: true,
      warnings: false,
      output: {
        comments: false
      }
    }
  })
);

// Sanity checks
//-------------------------------
if (config.devtool === 'eval') {
  throw new Error('webpack: using "eval" source-maps may break the build');
}

// Compile everything for PROD
//-------------------------------
console.info('webpack: running production build...');

const compiler = webpack(config);
compiler.run((err, stats) => {
  if (err) throw err;

  // Output stats
  console.log(
    stats.toString({
      colors: true,
      hash: false,
      chunks: false,
      version: true,
      chunkModules: true
    })
  );

  if (stats.hasErrors()) {
    console.warn('webpack: finished compiling webpack with errors...');
    console.warn(stats.compilation.errors.toString());
  } else {
    console.info('webpack: finished compiling webpack');
  }
});

module.exports = config;
