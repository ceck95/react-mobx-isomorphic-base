/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-12 15:38:16 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-04-20 14:29:59
 */
import React from 'react';
import { renderToString, renderToStaticMarkup } from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { Provider } from 'mobx-react';

import fetchData from './helpers/fetchData';

import Html from '../client/containers/server-side-render/Html.jsx';

import { createServerState } from '../client/state';
import App from '../client/containers/App';
//configServer
const configServer = require('../../webpack/server.config');
// Get actions object
import actions from '../client/actions';

import asyncBootstrapper from 'react-async-bootstrapper';
import {
  AsyncComponentProvider,
  createAsyncContext
} from 'react-async-component';

//Handle html for client render
const env = process.env.NODE_ENV || 'development';
let scriptGA = '';
if (env === 'production')
  scriptGA = `<!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77763438-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-77763438-2');
  </script>`;

const genHTML = (res, state) => {
  console.log('Client rendering ...');
  const isPro = process.env.NODE_ENV === 'production';
  const linkStatic = isPro
    ? configServer.http.static[0].url
    : `http://localhost:${configServer.webpack.devPort}`;
  const versionCurrent = require('../../version.json').version;
  const parseLink = (kind, extname) => {
    return `${linkStatic}/${kind}${
      isPro ? `-${versionCurrent}` : ''
    }.${extname}`;
  };
  const fileName = configServer.webpack.fileName;
  const linkJS = parseLink(fileName.js, 'js');
  const linkCss = parseLink(fileName.css, 'css');
  return res.send(`<!DOCTYPE html>
  <html lang="vi">
      <head>
          <meta name="robots" content="NOINDEX,NOFOLLOW"/>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
          <title>${state.app.title}</title>
          <link rel="stylesheet" href="${linkCss}" />
      </head>
      <body>
          <div id="${configServer.react.containerId}"></div>
          <script type="application/javascript" src="${linkJS}"></script>
          ${scriptGA}
      </body>
  </html>`);
};

// Handles page rendering ( for isomorphic / server-side-rendering too )

const Home = require('../client/containers/pages/Home');
//----------------------
export default async (req, res) => {
  const state = createServerState();
  // Set host variable to header's host
  state.app.host = req.headers.host;
  if (!configServer.react.serverRender)
    if (req.device.type !== 'bot') return genHTML(res, state);
  // Prepare for routing
  console.log('Server side render ...');
  // Ensure you wrap your application with the provider.
  const app = (
    <AsyncComponentProvider>
      <Provider state={state} actions={actions}>
        <StaticRouter location={req.url} context={req}>
          <App />
        </StaticRouter>
      </Provider>
    </AsyncComponentProvider>
  );
  asyncBootstrapper(app).then(() => {
    const appString = renderToString(app),
      html = renderToStaticMarkup(<Html state={state} appString={appString} />);

    res.status(200).send(`<!DOCTYPE html> \n${html}`);
  });
};
