/*
 * @Author: Tran Van Nhut (nhutdev)
 * @Date: 2017-10-11 22:45:19
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2017-10-13 16:18:45
 */
/**
 * Execute fetchData methods for each component
 * @param renderProps
 * @param state - contains our state
 * @returns {Promise} - returns a promise
 */
export default (renderProps, state, actions) => {
  const params = renderProps.params;
  const query = renderProps.location.query;

  const fetchDataMethods = renderProps.components.filter(c => c.fetchData).map(c => c.fetchData);

  return Promise.all(fetchDataMethods.map(method => method({
    state,
    actions,
    query,
    params
  })));
};
