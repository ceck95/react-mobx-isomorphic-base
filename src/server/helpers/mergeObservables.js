/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-11 15:32:36 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2017-10-13 23:09:48
 */

const mobx = require('mobx');
const isObservableArray = mobx.isObservableArray;
const isObservableMap = mobx.isObservableMap;
/**
 * Helper function that supports merging maps
 * @param target
 * @param source
 */

module.exports = function mergeObservables(target, source) {
  if (!source) {
    source = {};
    window.__CLIENT = true;
    return window.__STATE__ = target;
  }

  Object.keys(target).forEach(function(key) {
    if (typeof target[key] === 'object') {
      if (isObservableMap(target[key])) return target[key].merge(source[key]);
      if (isObservableArray(target[key])) return target[key].replace(source[key]);
      target[key] = source[key];
    } else {
      target[key] = source[key];
    }
  });

  return window.__STATE__ = target;
};