export default class Meta {

  static addMeta(data) {
    const append = (e) => {
      let meta = document.createElement(data.keyMeta || 'meta');
      meta.setAttribute(data.key, e);
      meta.setAttribute(data.keyContent || 'content', data.content);
      document.getElementsByTagName('head')[0].appendChild(meta);
    };
    if (data.keyData && Array.isArray(data.keyData))
      data.keyData.forEach(e => {
        append(e);
      });
    else
      append(data.keyData);
  }

  static addMetaString(data) {
    let result;
    const append = (e) => {
      result = `<${data.keyMeta || 'meta'} ${data.key}="${e}" ${data.keyContent || 'content'}="${data.content}" />`;
    };
    if (data.keyData && Array.isArray(data.keyData))
      data.keyData.forEach(e => {
        append(e);
      });
    else
      append(data.keyData);
    return result;
  }

  static parseMeta(meta, string) {
    const addMeta = Meta[string ? 'addMetaString' : 'addMeta'];
    let result = [];
    Object.keys(meta).forEach(e => {

      if (e === 'link') {

        Object.keys(meta[e]).forEach(a => {
          const item = addMeta({
            keyMeta: 'link',
            key: 'rel',
            keyData: a,
            keyContent: 'href',
            content: meta[e][a]
          });
          result.push(item);
        });

      } else {

        Object.keys(meta[e]).forEach(a => {
          const item = addMeta({
            key: e,
            keyData: a,
            content: meta[e][a]
          });
          result.push(item);
        });

      }

    });

    if (string)
      return result.join('');
  }

}