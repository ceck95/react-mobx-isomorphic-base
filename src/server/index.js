/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-11 22:32:54 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-04-20 10:22:20
 */
const path = require('path');
const webpackConfig = path.join(
  __dirname,
  `../../webpack/webpack.config.${
    process.env.NODE_ENV === 'production' ? 'prod' : 'dev'
  }`
);
try {
  const childProcess = require('child_process').spawn('node', [webpackConfig]);
  childProcess.stdout.setEncoding('utf8');
  childProcess.stdout.on('data', data => {
    console.log(data);
  });

  childProcess.on('error', err => {
    console.log(err);
  });
} catch (err) {
  console.log('exception: ' + err);
}

require('./server.js');
