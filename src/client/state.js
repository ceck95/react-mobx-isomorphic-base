/**
 * @Author: Tran Van Nhut (nhutdev)
 * @Date: 2017-10-11 15:37:17
 * @Email:  tranvannhut4495@gmail.com
 * @Last modified by:   Tran Van Nhut (nhutdev)
 * @Last modified time: 2017-10-11 15:37:17
 */

import {
  observable,
  toJS
} from 'mobx';
import mergeObservables from '../server/helpers/mergeObservables';


const common = {
    description: 'Full-stack developer, Information System, Freelancer - I have a passion create products awesome.I like to research new technology',
    url: 'https://nhutdev.com',
    author: 'nhutdev'
  },
  defaultState = observable({
    app: {
      title: common.author,
      meta: {
        name: {
          description: common.description,
          'dc.language': 'VN',
          locale: 'vi_VN',
          'dc.source': common.url,
          'dc.creator': 'Tran Van Nhut',
          distribution: 'Global',
          revisit: '1 days',
          'geo.placename': 'Vietnamese',
          'geo.region': 'Vietnamese',
          generator: common.author
        },
        property: {
          'og:type': 'website',
          'og:locale': 'vi_VN',
          'og:site_name': common.author,
          'og:url': common.url,
          'org:title': common.author,
          'og:description': common.description,
          'description': common.description,
          'twitter:description': common.description
        },
        link: {
          canonical: common.url
        }
      }

    },
    browse: {
      data: null,
      home: null
    }
  });

// Export function that creates our server state
export const createServerState = () => toJS(defaultState);
// Export function that creates our client states
export const createClientState = () => mergeObservables(defaultState, window.__STATE__);