import { asyncComponent } from 'react-async-component';
import React from 'react';

export const withAsync = (opt = {}) => {
  return component => {
    opt = {
      resolve: async function(props) {
        try {
          let data = await component.asyncLoad.call(props);
          return props => {
            return React.createElement(component, { data, ...props });
          };
        } catch (err) {
          console.log(err);
        }
      },
      ErrorComponent: ({ error }) => {
        console.log(error);
      },
      LoadingComponent: () => <div>Loading</div>,
      ...opt
    };

    return asyncComponent(opt);
  };
};
