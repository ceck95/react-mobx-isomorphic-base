import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'mobx-react';
import { Router } from 'react-router-dom';

import { createClientState } from './state';
import App from './containers/App.jsx';

import autorun from './autorun.js';

// Get actions object
import actions from './actions';

const config = require('../../webpack/server.config');

// Import our styles
import './assets/css/index.scss';

// Initialize stores
const state = createClientState();
let ignoreFirstLoad = true;

//Import ga
// import ReactGA from 'react-ga';
// ReactGA.initialize('UA-77763438-2');
import { createBrowserHistory } from 'history';
const history = createBrowserHistory();

class Index {
  constructor() {
    autorun(state);
  }

  createElement(props) {
    return (
      <Provider state={state} actions={actions}>
        <RouterContext {...props} />
      </Provider>
    );
  }

  render() {
    render(
      <Router history={history}>
        <Provider state={state} actions={actions}>
          <App />
        </Provider>
      </Router>,
      document.getElementById('root')
    );
  }
}

new Index().render();
