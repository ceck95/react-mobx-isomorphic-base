/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-11-03 14:14:09 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-04-19 14:17:37
 */

const configServer = require('../../../../webpack/server.config');
const fileName = configServer.webpack.fileName;
import helperMeta from '../../helpers/meta';
import Meta from './Meta.jsx';
import Link from './Link.jsx';

import React from 'react';
import PropTypes from 'prop-types';

/**
 * The is the HTML shell for our React Application.
 */
function HTML(props) {
  const { appString, state } = props;
  const version = require('../../../../version.json').version;
  const isProd = process.env.NODE_ENV === 'production';
  const devServerURL = isProd
    ? '/public'
    : 'http://' +
      state.app.host.replace(
        configServer.http.port,
        configServer.webpack.devPort
      );

  const metadata = {
    title: state.app.title
  };

  const parseLink = (kind, extname) => {
    return `${devServerURL}/${kind}${isProd ? `-${version}` : ''}.${extname}`;
  };

  // Inject state ( used for mobx-connect )
  const injectedState =
    'window.__STATE__ = ' + JSON.stringify(state, null, isProd ? 0 : 4) + ';';
  const storeMeta = helperMeta.parseMeta(state.app.meta, true);
  return (
    <html lang="vi">
      <head>
        <title>{metadata.title}</title>
        <meta name="robots" content="INDEX, FOLLOW" />
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"
        />
        {storeMeta.meta.map((e, i) => {
          return <Meta key={i} data={e} />;
        })}
        {storeMeta.link.map((e, i) => {
          return <Link key={i} data={e} />;
        })}
        <link href={parseLink(fileName.css, 'css')} rel="stylesheet" />
        <link rel="icon" type="image/x-icon" href="/favicon.ico" />
        <script dangerouslySetInnerHTML={{ __html: injectedState }} />
      </head>
      <body>
        <div
          id={configServer.react.containerId}
          dangerouslySetInnerHTML={{
            __html: props.appString
          }}
        />
      </body>
    </html>
  );
}

export default HTML;
