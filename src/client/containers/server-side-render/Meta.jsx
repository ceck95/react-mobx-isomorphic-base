import React, { Component } from 'react';

export default class Meta extends Component {

  constructor() {
    super();
  }

  render() {
    return <meta {...this.props.data}/>;
  }

}