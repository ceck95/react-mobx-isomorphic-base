/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-16 10:37:33 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-04-19 15:25:10
 */
import React, { Component } from 'react';
import { action } from 'mobx';
import { inject, observer } from 'mobx-react';
import {withAsync} from '../../ssr/withAsync'; 

@inject('state','actions')
@withAsync()
@observer
class NotFound extends Component {
  constructor() {
    super();
    this.state = {
      text: 'Not Found'
    };
  }

  //   componentWillReact() {
  //     console.log("I will re-render, since the todo has changed!");
  // }

  componentWillMount() {
    console.log(this.props);
  }

 static async asyncLoad() {
   console.log(this.actions,'test');
    // state.app.title = 'Not found';
    return await this.actions.load(this.state);
  }
  
 async load(){
    await this.props.actions.load(this.props.state,'test');
  }

  render() {
    return (
      <div className="columns">
        <div className="column" onClick= {this.load.bind(this)}>
          <h1>{this.props.state.browse.home}</h1>
        </div>
      </div>
    );
  }
}

export default NotFound;
