/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-15 10:53:56 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-04-20 15:19:16
 */
import React from 'react';
import { withAsync } from '../../ssr/withAsync';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';

@inject('state', 'actions')
@withAsync()
@observer
export default class Home extends React.Component {
  static async asyncLoad() {
    // return await this.actions.load(this.state);
  }

  render() {
    const { state } = this.props;
    return (
      <div className="main">
        <div className="bio">
          <div className="profile-img">
            <img src="public/images/avatar.jpg" />
          </div>

          <h3 className="header">Tran Van Nhut</h3>
          <p>
            Hello, I’m a full-stack developer. I have a passion create products
            awesomely. I want to research new technology.
            <br />
            Hobbies are research solution about database, pattern back-end,
            microservices and every thing problem to serve big users.
          </p>
          <Link className="bio-link" target="_blank" to="https://bitbucket.org/nhutjs/">
            Bitbucket<i className="fa fa-bitbucket" />
          </Link>
          <Link className="bio-link" target="_blank" to="https://github.com/nhutdev">
            Github<i className="fa fa-github" />
          </Link>
          <Link className="bio-link" target="_blank" to="https://bitbucket.org/nhutjs/">
            Get in touch with me<i className="fa fa-envelope" />
          </Link>
          <Link className="bio-link" target="_blank" to="public/cv/TranVanNhut-10-4-2018.pdf">
            Curriculum Vitae<i className="fa fa-book" />
          </Link>
        </div>
        <div className="contact">
          <ul className="skills">
            <li className="skill" aria-label="nodejs">
              NodeJS
            </li>
            <li className="skill" aria-label="reactjs">
              ReactJS
            </li>
            <li className="skill" aria-label="php">
              PHP
            </li>
            <li className="skill" aria-label="front">
              HTML, CSS, CSS3
            </li>
            <li className="skill" aria-label="database">
              MongoDB, PostgreSQL, MySQL, Redis, ...
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
