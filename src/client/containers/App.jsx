import React, { Component } from 'react';
import Menu from '../components/Menu.jsx';
import Footer from '../components/Footer.jsx';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { inject } from 'mobx-react';

/**
 * Asynchronously load a file
 * @param main {String} - Main component
 * @returns {Function}
 */

const Home = require('./pages/Home.jsx');
const NotFound = require('./pages/NotFound.jsx');

@inject('actions', 'state')
class App extends Component {
  render() {
    return (
      <div className="container">
        <Switch>
          <Route exact path={'/'} component={Home} />
          <Route path="*" component={NotFound} />
        </Switch>
      </div>
    );
  }
}
App.propTypes = {
  children: PropTypes.any
};

import { withRouter } from 'react-router-dom';

export default withRouter(App);
