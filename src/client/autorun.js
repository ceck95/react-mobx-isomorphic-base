import * as mobx from 'mobx';
import helperMeta from './helpers/meta';

export default state =>
  mobx.autorun(() => {
    if (window.__CLIENT) {
      document.title = state.app.title;
      const meta = state.app.meta;
      helperMeta.parseMeta(meta);
    }
  });
