import React, { Component } from 'react';

export default class Info extends Component {

  constructor() {
    super();
  }

  render() {
    return (
      <div className="columns">
        <div className="column">
          <p>
            { this.props.data.key }
          </p>
        </div>
        <div className="column">
          <p>
            { this.props.data.value }
          </p>
        </div>
      </div>
      );
  }

}