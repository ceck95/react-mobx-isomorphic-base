/*
 * @Author: Tran Van Nhut (nhutdev) 
 * @Date: 2017-10-13 13:43:55 
 * @Last Modified by: Tran Van Nhut (nhutdev)
 * @Last Modified time: 2018-04-05 13:52:21
 */
import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default class Menu extends React.Component {
  static get propTypes() {
    return {
      data: PropTypes.any
    };
  }

  render() {
    return (
      <nav className="navbar navbar--margin">
        <Link className="navbar-item no-hover" to="/">
        <p className="navbar-item__text">nhutdev.com</p>
        </Link>
      </nav>
      );
  }
}