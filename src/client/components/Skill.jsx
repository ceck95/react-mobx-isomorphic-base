import React, { Component } from 'react';

export default class Skill extends Component {

  render() {
    return (
      <div className="column skill">
        <img className="img-size-skill" src={ this.props.data.img } />
        <p>
          { this.props.data.desc }
        </p>
      </div>
      );
  }

}

